# AISCO Feature Model
Feature model digunakan pada Software Product Line Engineering (SPLE)
untuk menggambarkan _commonality_ dan _variability_ dari suatu product line.
Salah satu representasi dari feature model adalah feature diagram,
yang menunjukkan visualisasi pemodelan fitur dalam bentuk hierarki _tree_. 
Project ini berisi feature diagram dari AISCO yang digambar dengan 
menggunakan FeatureIDE. 

## Feature Diagram
Pada project ini, terdapat 2 jenis feature model, yaitu:
- [Complete Feature Diagram](/featureaisco) <br />
Feature diagram pada direktori ini merupakan model lengkap dari kebutuhan AISCO.

- [Part 1 Feature Diagram](/part-aiscofm) <br />
Feature diagram pada direktori ini merupakan sebagaian feature diagram AISCO
yang sedang dalam proses untuk diimplementasikan





